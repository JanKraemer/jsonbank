# JSONBank
## Scans your inventory and converts it to simple and neat JSON format
Command to run scan and popup window with JSON format of your inventory is 
```
/run doScan();
```
Note that __there isn't _any_ recognition whether the bank window is opened__, so if it isn't, output would be just like if your bank was completely empty, even without any bags.
